# Traceroute the way
## Manuele Trimarchi's italian weblog

Well well, I finally made all my blog available under Jekyll.

## Technologies I'm using
- [Jekyll](http://jekyllrb.com/)
- [Poole](https://github.com/poole/poole)
- [GitHub Pages](https://pages.github.com/)
- [Rake](https://github.com/ruby/rake)
- [lunr.js](http://lunrjs.com/)

## Run, debug, study my shit
Just download this repository, and serve the content using the Jekyll built-in webserver.

(Updates are coming: now I manage all my blogging infrastructure using Grunt. Stay tuned if you are interested in this!)

```bash
$ git clone git@github.com:mtrimarchi/mtrimarchi.github.io.git
$ cd mtrimarchi.github.io
$ jekyll serve
```

